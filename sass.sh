#!/bin/bash
set -e
set -o pipefail

mkdir -p dist

if [ "x$1" = "x" ]; then
    sass src/scss/app.scss src/dist/app.css
else
    sass --watch src/scss/app.scss:src/dist/app.css
fi
