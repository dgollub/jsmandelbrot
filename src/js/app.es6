/*
    ES6 code entry point
*/

import { getRandomInt, rgbToHex, hexToRgb, isDarkColor } from './utils.es6';

require('babel-polyfill');

let mathjs = require("mathjs");

const VERSION = "0.0.1"

console.log(`Version: ${VERSION}`);


let draw = () => {
    console.log("draw");
    if (looper == null) {
        looper = gen();
    }
    let n = looper.next();
    if (!n.done) {
        let image = n.value;
        ctx.putImageData(image, 0, 0);
    }
};

let gen = function* () {
    console.log("gen");
    let step = 0;
    let [w, h] = [ctx.width, ctx.height];
    let image = ctx.getImageData(0, 0, w, h);
    let data = image.data;
    let maxSteps = w * h; // each pixel counts!
        
    let init = () => {
        [w, h] = [ctx.width, ctx.height];
        maxSteps = w * h; // each pixel counts!
        ctx.clearRect(0, 0, w, h);
        ctx.fillStyle = "#FF0000";
        ctx.fillRect(10, 10, 150, 75);
        image = ctx.getImageData(0, 0, w, h);
        data = image.data;
    }

    init();

    let yieldStep = 250;
    
    while (true) {
        // TODO(dkg): figure out how to calc x, y and how to represent complex numbers on the y axis.
        // inspiration: https://github.com/cslarsen/mandelbrot-js
        let yieldCurrent = 0;
        for (var x = 0; x < w; x++) {
            for (var y = 0; y < h; y++) {
                yieldCurrent++;
                // TODO(dkg): figure out a way to do this without mathjs and complex numbers ... sigh
                let c1 = 1.0 * (x - (w - h) / 2.0) / h * 4.0 - 2.0;
                let c2 = 1.0 * (y / h * 4.0) - 2.0;
                let c = mathjs.complex(c1, c2);
                let z = mathjs.complex(0, 0);
                for (var color = 0; color < 255; color++) {
                    z = mathjs.add(mathjs.multiply(z, z), c);
                    if (mathjs.abs(z) > 2.0) {
                        break;
                    }
                }
                let index = ((w * y) + x) * 4;
                data[index] = color;  // red
                data[index + 1] = color; // green
                data[index + 2] = color; // blue
                data[index + 3] = 255; // alpha
                
                if (yieldCurrent > yieldStep) {
                    let reset = yield image;
                    
                    step++;
                    if (step >= maxSteps) {
                        reset = true;
                    }
                    
                    if (reset) {
                        init();
                    }
                    yieldCurrent = 0;
                }
            }   
        }
    }
};

let drawLoop = () => {
    draw();
    requestAnimationFrame(drawLoop);
};

let canvas;
let ctx;
let looper = null;

// main entry point - get this party started
$(() => {
    console.log("Get this party started.");
    canvas = document.getElementById("board");
    ctx = canvas.getContext("2d");
    ctx.width = canvas.width;
    ctx.height = canvas.height;
    
    drawLoop();
});
